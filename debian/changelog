powermanga (0.93.1-6) unstable; urgency=medium

  * Team upload
  * Explicitly build-depend on libpng-dev.
    Historically this was pulled in via a dependency chain from
    libsdl1.2-dev, but with libsdl1.2-compat-dev that's no longer the case.
    This package explicitly checks for <png.h>, so it should have the
    build-dependency itself, whether it's also a transitive dependency
    or not. (Closes: #1039575)
  * Update watch file format version to 4
  * Update standards version to 4.6.2 (no changes needed)

 -- Simon McVittie <smcv@debian.org>  Wed, 28 Jun 2023 13:39:39 +0100

powermanga (0.93.1-5) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.1.
  * Do not run deduplication on documentation because this causes Lintian error
    symlink-target-in-build-tree.
  * Remove Sam from Uploaders because he is not active anymore.
    (Closes: #1011586)

 -- Markus Koschany <apo@debian.org>  Sat, 27 Aug 2022 15:00:28 +0200

powermanga (0.93.1-4) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.
  * Mark powermanga-data Multi-Arch: foreign.

 -- Markus Koschany <apo@debian.org>  Fri, 01 Jan 2021 22:31:45 +0100

powermanga (0.93.1-3) unstable; urgency=medium

  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.1.
  * Use canonical VCS URI.
  * Remove get-orig-source target.
  * Drop deprecated menu file.
  * Use https for Format field.

 -- Markus Koschany <apo@debian.org>  Fri, 07 Dec 2018 22:36:24 +0100

powermanga (0.93.1-2) unstable; urgency=medium

  * Switch to compat level 10.
  * Declare compliance with Debian Policy 3.9.8.
  * Update my email address.
  * Remove dh-autoreconf from Build-Depends.
  * Vcs-Git: Use https.
  * Update copyright years.

 -- Markus Koschany <apo@debian.org>  Mon, 23 Jan 2017 14:56:30 +0100

powermanga (0.93.1-1) unstable; urgency=medium

  * Imported Upstream version 0.93.1.
    - Clears all gems after the final boss. Thanks to Josh Triplett for the
      report. (Closes: #764009)
    - Disable insecure temporary file "/tmp/powermanga-log.txt". Thanks
      to Josh Triplett for the report (Closes: #764144)
  * Drop no-pedantic-build.patch. Fixed upstream.
  * Add get-orig-source target.

 -- Markus Koschany <apo@gambaru.de>  Wed, 01 Jul 2015 13:36:05 +0200

powermanga (0.93-2) unstable; urgency=medium

  * Moved the package to Git.
  * Add no-pedantic-build.patch.
    Do not treat compiler warnings as errors. Fixes FTBFS with GCC-5.
    (Closes: #778072)

 -- Markus Koschany <apo@gambaru.de>  Thu, 25 Jun 2015 20:07:22 +0200

powermanga (0.93-1) unstable; urgency=medium

  * Imported Upstream version 0.93.
  * Drop all existing patches. Merged upstream.
  * Declare compliance with Debian Policy 3.9.6.
  * Install new french man page with manpages file.
  * debian/rules: Disable hardening option stackprotectorstrong because it
    causes a segmentation fault on i386 and makes the game unusable. However
    stackprotector seems to work fine, so keep it for the time being.

 -- Markus Koschany <apo@gambaru.de>  Fri, 03 Oct 2014 14:12:39 +0200

powermanga (0.92-2) unstable; urgency=medium

  * Add joystick.patch:
    Fixes wrong joystick behaviour in display_sdl.c and allows the ship to move
    to the left side again.
    Thanks to Kalle Olavi Niemitalo and Bruno Ethvignot for the report, testing
    and the patch. (Closes: #561670)

 -- Markus Koschany <apo@gambaru.de>  Sun, 07 Sep 2014 23:48:55 +0200

powermanga (0.92-1) unstable; urgency=medium

  * Imported Upstream version 0.92. (Closes: #734438)
    - Fixes random segfaults in powermanga.
      (Closes: #478213) (LP: #1264677)
  * Add myself to Uploaders.
  * Switch to source format 3.0 (quilt).
  * Use compat level 9 and require debhelper >= 9
  * wrap-and-sort -sa.
  * Switch to dh sequencer and simplify debian/rules.
  * Build with parallel and dh-autoreconf. (Closes: #727946)
  * debian/patches:
    - Drop 005_link_libm.diff.
      The new upstream version links explicitly against libm.
      Thanks to Julian Taylor for the patch. (Closes: #632945)
    - Drop 020_copyright.diff. Fixed upstream.
    - Drop 030_manpage_typo.diff. Fixed upstream.
    - Drop 010_scoredir_path.diff. Fixed upstream
    - Add desktop-file.patch. Add keywords and a comment in German.
    - Add manpage.patch. Fullscreen mode can be enabled by using the
      --fullscreen option not --full.
    - Add support-custom-CFLAGS.patch. Pass all hardening options from
      dpkg-buildflags to the build.
    - Add segmentation-fault-about_en.patch: (Closes: #760146)
      Fix a segfault in the English About menu. Thanks to Bruno Ethvignot.
  * debian/control:
    - Remove versioned dependency for libsdl1.2-dev. It is trivially satisfied.
    - Remove build-dependency on quilt. Source format 3.0 uses quilt by
      default.
    - Declare compliance with Debian Policy 3.9.5.
    - Remove Conflicts field for suidmanager. Obsolete.
    - Add dh-autoreconf to Build-Depends.
  * Drop *.dirs files. Not needed anymore.
  * Drop powermanga-data.preinst. Obsolete.
  * Delete debian/powermanaga.desktop. Use the one provided by upstream
    instead.
  * Drop README.source. We use source format 3.0 now.
  * Delete debian/powermanga.xpm. Use the icon from upstream instead.
  * Use install files instead of dh_install commands in debian/rules.
  * Add longtitle to powermanga.menu.
  * Add powermanga.links and link powermanga-data's doc directory to powermanga.
  * Update debian/copyright to copyright format 1.0.
  * debian/watch: Make the extension regex more flexible.
  * Install html documentation to /usr/share/doc/powermanga-data.
    (Closes: #525602)
  * Register documentation with doc-base.
  * Remove duplicate images from documentation. Use rdfind and symlinks to
    reduce the duplication.
  * Clean up postinst and postrm script and remove obsolete code. Remove
    hi-score files only when purging the package.

  [Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org.

 -- Markus Koschany <apo@gambaru.de>  Tue, 02 Sep 2014 16:02:25 +0200

powermanga (0.90-dfsg-2) unstable; urgency=low

  [ Cyril Brulebois ]
  * Add missing context to the 020_copyright.diff patch, so as to prevent
    an FTBFS with the new “3.0 (quilt)” source package format, thanks to
    Raphaël Hertzog for the notice (Closes: #485021).

  [ Barry deFreese ]
  * Update my e-mail address.
  * 005_link_libm.diff - Add -lm to LDFLAGS. (Closes: #556072).
  * 030_manpage_typo.diff - Fix typo in manpage. (Closes: #455227).
  * Update postinst to make powermanga setgid games. (Closes: #419572).
    + Thanks to Javier Fernández-Sanguino Peña for the fix.
  * Add dversionmangle in watch file to remove -dfsg version.
  * Add set -e to postrm.
  * Clean the quilt dir on clean.
  * Add README.source for quilt.
  * Bump Standards Version to 3.8.3. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Fri, 13 Nov 2009 14:28:30 -0500

powermanga (0.90-dfsg-1) unstable; urgency=low

  [ Gonéri Le Bouder ]
  * add desktop file, thanks Adrien Cunin (Closes: #402168)
   - powermanga.xpm moved in /usr/share/pixmaps
  * call dh_desktop to run update-desktop-database

  [ Cyril Brulebois ]
  * Added XS-Vcs-Svn and XS-Vcs-Browser fields in the control file.

  [ Barry deFreese ]
  * Bump debhelper build-dep version to match compat
  * Fix substvar source:Version
  * Make distclean not ignore errors
  * Add watch file
  * Add Homepage field in control
  * Remove XS- from VCS fields in control
  * New Upstream Release (Closes: #447415)
    + Should now be dfsg free
    + Adds joystick support (Closes: #442322)
    + STILL NOT SURE ABOUT tlk.fnt
  * Revert patches except fixing scoredir path and copyright for manpage
  * Remove deprecated Encoding tag from desktop file
  * Remove file extension from icon tag in desktop file
  * Replace evil 'pwd' with $(CURDIR)
  * Removing config.log config.status in clean target
  * Convert copyright to UTF-8.
  * Remove order/ dir from upstream tarball.
  * Remove empty dirs from powermanga package.
  * Bump Standards Version to 3.7.3

 -- Barry deFreese <bddebian@comcast.net>  Wed, 19 Mar 2008 21:32:41 -0400

powermanga (0.80-dfsg-1) unstable; urgency=high

  * Repackage previous upload as non-native.
  * Acknowledge NMU (Closes: #384223).
  * debian/control:
    + Add missing build-depends on zlib1g-dev (Closes: #397707).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu,  9 Nov 2006 11:15:24 +0100

powermanga (0.80+dfsg-1) unstable; urgency=medium

  * Replaced non-free sounds with free replacements by
    David Igreja from TLK (upstream):
    http://linux.tlk.fr/games/Powermanga/download/sounds-powermanga-update-20060912.tgz
    (Closes: #384223) (RC bugfix, urgency medium)

 -- Moritz Muehlenhoff <jmm@debian.org>  Wed, 11 Oct 2006 00:14:34 +0200

powermanga (0.80-3) unstable; urgency=low

  * debian/rules:
    + Manually install upstream's required text files (Closes: #367651).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu, 18 May 2006 05:01:01 +0200

powermanga (0.80-2) unstable; urgency=low

  * debian/powermanga-data.preinst:
    + Fixed a typo in the powermanga/powermanga-data symlink transition.
    + Add set -e to trap errors.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue, 16 May 2006 16:56:24 -0500

powermanga (0.80-1) unstable; urgency=low

  * New upstream release.
  * This release merges a few Debian patches; updated debian/patches
    accordingly.
  * debian/powermanga.postrm:
    + Send dpkg-statoverride output to /dev/null.
  * debian/powermanga.postinst:
    + Minor fix in the powermanga/powermanga-data symlink transition.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue, 16 May 2006 19:11:19 +0200

powermanga (0.79-5) unstable; urgency=low

  * Moved packaging to the Debian Games Team.
  * Use quilt for patch management.
  * debian/control:
    + Set policy to 3.7.2.
    + Build-depend on quilt.
  * debian/rules:
    + Various rules cleanup.
    + Fixed the powermanga/powermanga-data dependency loop.
  * debian/copyright:
    + Added missing names in the authors list.

  * debian/patches/000_data_path.diff:
    + New patch from old diff.gz -- fix data paths in autotools files.

  * debian/patches/010_rebootstrap.diff:
    + New patch from old diff.gz -- rebootstrap package.

  * debian/patches/020_copyright.diff:
    + New patch from old diff.gz -- fix my copyright in some files.

  * debian/patches/020_warning.diff:
    + New patch from old diff.gz -- fix a compilation warning.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue, 16 May 2006 01:34:47 +0200

powermanga (0.79-4) unstable; urgency=low

  * debian/control:
    + Set policy to 3.6.2.1.
    + Build-depend on libxxf86dga-dev, libxxf86vm-dev instead of
      xlibs-static-dev (Closes: #323905).
  * src/linuxroutines.cpp:
    + Removed a useless cast from void* to int (Closes: #287922).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Wed, 24 Aug 2005 14:12:38 +0200

powermanga (0.79-3) unstable; urgency=low

  * debian/control:
    + Reverted the libxt-dev build dependency and hardcoded X11 path. I will
      fix this properly when I have enough bandwidth for pbuilder.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Mon,  2 Aug 2004 10:42:48 +0200

powermanga (0.79-2) unstable; urgency=low

  * src/gfxroutines.cpp:
    + Fixed a patch badly backported from 0.78 (Closes: #262857).
  * debian/control:
    + Build-depend on libxt-dev, which we don't actually use, but which
      configure needs when checking for X libraries (Closes: #262856).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Mon,  2 Aug 2004 08:45:58 +0200

powermanga (0.79-1) unstable; urgency=low

  * New upstream release.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Sun,  1 Aug 2004 19:36:37 +0200

powermanga (0.78-6) unstable; urgency=low

  * debian/control:
    + Set policy to 3.6.1.1.
    + Build-depend on libx11-dev, libxext-dev, xlibs-static-dev instead of
      xlibs-dev.
    + Build-depend on debhelper (>= 4.0).
  * debian/powermanga.menu:
    + Quoted strings where appropriate.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu, 29 Jul 2004 18:23:16 +0200

powermanga (0.78-5) unstable; urgency=low

  * debian/rules:
    + The hiscore location changed; fixed installation (Closes: #218253).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu, 30 Oct 2003 10:37:14 +0100

powermanga (0.78-4) unstable; urgency=low

  * debian/control:
    + Minor enhancement in the short description.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Mon, 27 Oct 2003 20:36:15 +0100

powermanga (0.78-3) unstable; urgency=low

  * debian/control:
    + Set policy to 3.6.1.0.
    + Wrote more meaningful long descriptions (Closes: #209859).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Thu,  2 Oct 2003 15:27:08 +0200

powermanga (0.78-2) unstable; urgency=low

  * src/Makefile.am: Fixed the scorefile location (Closes: #203248).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue, 29 Jul 2003 15:36:30 +0200

powermanga (0.78-1) unstable; urgency=low

  * New upstream release.
    + Includes the Debian cross-platform C port.
    + Fully uses autoconf/automake.
    + Uses SDL_mixer instead of libseal, which fixes problems with sound
      systems not supported by libseal (Closes: #129062, #161255).
  * debian/control:
    + Added a build dependency to libsdl1.2-dev.
    + Changed build dependency from libseal-dev to libsdl-mixer1.2-dev.
    + Set policy to 3.6.0. No changes required.
    + Updated debhelper build-dependency to (>= 3.4.4) because we now
      use debian/compat.
    + Use ${misc:Depends} everywhere.
  * debian/rules:
    + Removed many compilation flags that are not needed any more.
    + Added usual touch magic to avoid autotools timestamp issues.
  * debian/docs:
    + Removed README from the packages, it only contains installation
      instructions.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue,  1 Jul 2003 23:36:44 +0200

powermanga (0.74-6) unstable; urgency=low

  * Removed hardcoded calls to g++-3.2 (Closes: #196274).
  * debian/control:
    + Set policy to 3.5.10.
    + Removed the leading "a" in the package description.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Fri,  6 Jun 2003 00:37:23 +0200

powermanga (0.74-5) unstable; urgency=low

  * Removed all x86-only gcc flags from the Makefile.

 -- Samuel Hocevar <sam@zoy.org>  Wed, 23 Apr 2003 05:10:26 +0200

powermanga (0.74-4) unstable; urgency=low

  * Wrote routines.cpp which is a C version of assembler.S. Only tested on
    my x86, colour palette is probably broken on big-endian architectures,
    and certainly broken on 64 bits CPUs.
  * The powermanga package is now Architecture: any.

 -- Samuel Hocevar <sam@zoy.org>  Sat, 15 Mar 2003 20:30:11 +0100

powermanga (0.74-3) unstable; urgency=low

  * "But how will I know when I have received enlightenment?" asked the novice.
    "Your program will then run correctly," replied the master.
  * Complete, total, absolute fix of the dpkg-statoverride brokenness. The
    high-score files can now be owned by root, the main binary needs not be
    setgid games in the package, and postinst/postrm leave the statoverride
    settings like they should be. And we handle upgrades from old broken
    versions of the package.
  * We remove score files upon package purge.
  * Bumped build-dependency on debhelper (>=3.0).

 -- Samuel Hocevar <sam@zoy.org>  Sat, 15 Mar 2003 20:30:11 +0100

powermanga (0.74-2) unstable; urgency=low

  * Updated standards version.
  * Added debhelper token to maintainer scripts.
  * Fixed a big memory leak in gardiens.cpp that was eating 64 memory slots
    on each new game and every 4 levels (Closes: #156265).
  * Fixed a minor memory leak in courbe.cpp and grille.cpp.
  * Added missing close() calls in error handling in linuxroutines.cpp.
  * CXXFLAGS is now gcc3.2 compliant.
  * Redesigned the icon so that it only uses the 24 Debian colours.

 -- Samuel Hocevar <sam@zoy.org>  Sat, 15 Mar 2003 20:30:11 +0100

powermanga (0.74-1) unstable; urgency=low

  * New upstream version, includes latest Debian patches.
  * Added year to debian/copyright (Closes: #159430).
  * Fixed minor typo in debian/changelog (Closes: #159432).
  * postinst now removes overrides (Closes: #161860).
  * Makefile now uses gcc 3.2.
  * Resized icon to 32x32.

 -- Samuel Hocevar <sam@zoy.org>  Sat,  8 Feb 2003 02:37:43 +0100

powermanga (0.73-1) unstable; urgency=low

  * New upstream version.
  * Fixed a sound initialization problem that could make Powermanga randomly
    silent when launched on a fast CPU.
  * There are now three difficulty levels.
  * Merged changes from joeyh's NMU (Closes: #155850).

 -- Samuel Hocevar <sam@zoy.org>  Sun, 18 Aug 2002 01:55:55 +0200

powermanga (0.71c-8.1) unstable; urgency=low

  * NMU that makes the program sgid games so it can write to its high score
    file. I also did a basic audit; nothing comprehensive. Closes: #126462

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Aug 2002 17:59:00 -0400

powermanga (0.71c-8) unstable; urgency=low

  * Spelling fixes in package description (Closes: #125263, #125264).
  * Changed build-depend from xlib6g-dev to xlibs-dev.

 -- Samuel Hocevar <sam@zoy.org>  Wed, 19 Dec 2001 17:41:43 +0100

powermanga (0.71c-7) unstable; urgency=low

  * Made all packages x86-only until all asm is removed, this time really
    fixing the non-x86 bug (Closes: #90543).
  * Updated user-contributed manpage (Closes: #91932).
  * Fixed spelling in fntscrol.cpp (Closes: #100457).
  * Fixed score files permissions (Closes: #85687).

 -- Samuel Hocevar <sam@zoy.org>  Tue,  3 Jul 2001 17:00:18 +0200

powermanga (0.71c-6) unstable; urgency=low

  * Made the package x86-only until all asm is removed (Closes: #84026).
  * We now unregister override files (Closes: #84815).

 -- Samuel Hocevar <sam@zoy.org>  Mon,  5 Feb 2001 20:49:15 +0100

powermanga (0.71c-5) unstable; urgency=low

  * Fixed preinst script (Closes: #82672).

 -- Samuel Hocevar <sam@zoy.org>  Wed, 17 Jan 2001 22:58:36 +0100

powermanga (0.71c-4) unstable; urgency=low

  * Rebuilt package using a fixed dpkg.

 -- Samuel Hocevar <sam@zoy.org>  Tue, 16 Jan 2001 21:42:37 -0500

powermanga (0.71c-3) unstable; urgency=low

  * Added versioned dependency for debhelper.

 -- Samuel Hocevar <sam@zoy.org>  Thu, 11 Jan 2001 00:07:08 +0100

powermanga (0.71c-2) unstable; urgency=low

  * Removed a useless Nautilius desktop file (Closes: #79533).

 -- Samuel Hocevar <sam@zoy.org>  Wed, 20 Dec 2000 04:46:31 +0100

powermanga (0.71c-1) unstable; urgency=low

  * Initial Release (Closes: #78214).

 -- Samuel Hocevar <sam@zoy.org>  Wed, 29 Nov 2000 04:36:47 +0100
